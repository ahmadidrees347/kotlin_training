open class A {
    lateinit var name : String
    var age : Int
    constructor(){
        name = "Ahmad"
        age = 0
    }
    fun getData() {
        println("Name: "+name);
        println("Age: "+age);
    }
}

class B : A() {
    var strName : String = ""
    init {
        strName = "Hi"
         name = strName
    }
}
open class Rectangle {
    val borderColor: String get() = "black"
    open fun demoFunction() { println("Parent demoFunction") }
}

class FilledRectangle : Rectangle() {
    val color: String get() = super.borderColor
    override fun demoFunction() {
        super.demoFunction()
        println("Child demoFunction")
    }

}
fun main(args: Array<String>){
    var a = B()
    a.getData()

    var fill = FilledRectangle()
    fill.demoFunction()
    println("Parent Color: "+fill.borderColor)
    println("Child Color: "+fill.color)

}