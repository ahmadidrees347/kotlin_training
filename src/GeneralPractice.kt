class GeneralPractice {
    fun NestedLoopWithBreakandContinue() {
        label1@
        for(i in 0..10 step 1){
            label@
            for(j in 0..10){
                if(i==5 && j==0) {
                    print(i.toString()+" "+j.toString()+"\n")
                    continue@label1
                }
                else {
                    print(i.toString()+" "+j.toString()+"\n")
                    break@label
                }
            }
        }
    }
    fun CollectionExample() {
        println("\nCollection Example")
        val fruits = listOf("banana", "avocado", "apple", "kiwifruit")
        //fruits.forEach { println(it) }
        fruits
            .filter { it.startsWith("a") }
            .sortedBy { it }
            .map { it.toUpperCase() }
            .forEach { println(it) }
    }
    fun switchReplaceWithWhen(x: Any) :String =
        when(x) {
            1 -> "One"
            2 -> "Two"
            3 -> "Three"
            "Hi" -> "Hi"
            else -> "Invalid Choice"
        }

    fun loops() {
        println("\nForEach In Kotlin ");
        val items = listOf("A", "B", "C", "D")
        //ForEach
        for (item in items) {
            println(item)
        }
        //While
        var index = 0
        while (index < items.size) {
            println("item at $index is ${items[index]}")
            index++
        }
        //For Loop with step 2
        for (x in 1..10 step 2) {
            print(" "+x+",")
        }
        print("\n")
        //For Loop with Range function step 2
        for (x in 1.rangeTo(10).step(2)) {
            print(" "+x+",")
        }
        print("\n")
        //Loop in reverse
        for (x in 9 downTo 0 step 3) {
            print(" "+x+",")
        }
        print("\n")
    }
    fun stringExample() {
        println("\nString Function In Kotlin ");
        var str : String = "I am a good Boy!"

        println("String is: "+str)
        if(str.contains("a"))
            println("Index of a is: "+str.indexOf("a"))
        println("Size of String is: "+str.length)
        println("Substring of String is: "+str.substring(0, str.length-2))
    }
    fun sum(a: Int, b: Int): Int? {
        return a + b
    }
    fun sub(a:Int, b:Int):Int? {
        return a-b;
    }
    fun mul(a:Int, b:Int):Int? {
        return a*b;
    }
    fun div(a:Int, b:Int):Int? {
        return a/b;
    }
    fun KotlinDataTypes() {
        val a: Int = 10000
        val d: Double = 100.00
        val f: Float = 100.00f
        val l: Long = 1000000004
        val s: Short = 10
        val b: Byte = 1

        println("\nData Types In Kotlin ");
        println("Your "+a::class.simpleName+" Value is "+a);
        println("Your "+d::class.simpleName+"  Value is "+d);
        println("Your "+f::class.simpleName+" Value is "+f);
        println("Your "+l::class.simpleName+" Value is "+l);
        println("Your "+s::class.simpleName+" Value is "+s);
        println("Your "+b::class.simpleName+" Value is "+b);
    }
    fun ArrayInit() {
        var array : IntArray = intArrayOf(0,1,2,3,4,5)
        println("\nValues In Array")
        for(i in 0..array.size-1) {
            println("Array at Index "+i+": "+array[i])
        }
    }
}

fun main(args: Array<String>) {

    var generalPractice = GeneralPractice();
    var str : String = "Hey!"

    if (str is String)
        println("$str Hello, world!!!")

    println("\nArithmatic Operations In Kotlin");
    println("Sum of two Number is: "+generalPractice.sum(9,10))
    println("Subtraction of two Number is: "+generalPractice.sub(9,10))
    println("Multiplication of two Number is: "+generalPractice.mul(9,10))
    println("Divion of two Number is: "+generalPractice.div(9,10))

    generalPractice.KotlinDataTypes()
    generalPractice.ArrayInit()
    generalPractice.stringExample()
    generalPractice.loops()
    //When Statement in Kotlin
    println("\nWhen Statement in Kotlin")
    println("You Enter: "+generalPractice.switchReplaceWithWhen(1))
    println("You Enter: "+generalPractice.switchReplaceWithWhen(2))
    println("You Enter: "+generalPractice.switchReplaceWithWhen(3))
    println("You Enter: "+generalPractice.switchReplaceWithWhen("Hi"))
    println("You Enter: "+generalPractice.switchReplaceWithWhen("Hmmm"))
    //Collection Example
    generalPractice.CollectionExample()
    generalPractice.NestedLoopWithBreakandContinue()

}