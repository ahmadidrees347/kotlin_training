class DemoSecondaryConstructor  {
    var name : String? = ""
    constructor(name1: String) {
        name = name1
        println("Secondary Constructor! $name")
        println("Length: ${name!!.length}")
    }
    init {
        name = "Ahmad"
        println("Secondary Constructor initializer, Name is: ${name}")
        println("Secondary Constructor, Length of Name is: ${name!!.length}")
    }
    fun getUserName(): String {
        return name.toString()
    }
}
class DemoPrimaryConstructor(name: String)  {

    init {
        println("Primary Constructor initializer, Name is: ${name}")
        println("Primary Constructor, Length of Name is: ${name!!.length}")
    }
}
fun main(args: Array<String>) {
    var str : String = "Ahmad!"

    var demoSClass = DemoSecondaryConstructor(str)
    println(demoSClass.getUserName())
    var demoPClass = DemoPrimaryConstructor(str)

}