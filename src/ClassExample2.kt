open class Employees {
    var id : Int? = -1
    var name : String? = ""
    var scale : Int? = -1
    var perHour : Int? = -1
    var workingHours : Int? = -1
    var experience : Int? = -1
    var salary : Double = 0.0
    constructor() {
        id  = -1
        name = ""
        scale = -1
        perHour = -1
        workingHours = -1
        experience = -1
        salary = 0.0
    }
    open fun calculateSalary() : Double {
        return 0.0
    }
}

class Salesman() : Employees() {
    init {
        perHour = 200
    }

    override fun calculateSalary(): Double {
        salary = workingHours!!.toDouble() * perHour!!.toDouble()
        return salary
    }
    constructor(empId : Int, empName : String, Salscale: Int, workingHour : Int) : this() {
        experience = empId
        name = empName
        scale = Salscale
        workingHours = workingHour
    }
}
class Manager() : Employees() {
    init {
        perHour = 500
    }
    override fun calculateSalary(): Double {
        salary = workingHours!!.toDouble() * perHour!!.toDouble()
        return salary as Double
    }
    constructor(empId : Int, empName : String, Salscale: Int, workingHour : Int) : this() {
        experience = empId
        name = empName
        scale = Salscale
        workingHours = workingHour
    }
}
class Supervisor() : Employees() {
    init {
        perHour = 300
    }
    override fun calculateSalary(): Double {
        //super.calculateSalary()
        salary = workingHours!!.toDouble() * perHour!!.toDouble()
        return salary
    }
    constructor(empId : Int, empName : String, Salscale: Int, workingHour : Int) : this() {
        experience = empId
        name = empName
        scale = Salscale
        workingHours = workingHour
        //super.
    }
}
fun main(args : Array<String>) {
    var emp1 = Salesman(1,"Nabeel", 21,123)
    println("Salary of "+emp1.name+" is: "+emp1.calculateSalary())
    var emp2 = Supervisor(2,"Umar", 21,123)
    println("Salary of "+emp2.name+" is: "+emp2.calculateSalary())
    var emp3 = Manager(3,"Ahmad", 21,123)
    println("Salary of "+emp3.name+" is: "+emp3.calculateSalary())
}