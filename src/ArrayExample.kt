class ArrayExample {
    fun traverseAlphabetsWithArray(vararg strArray: Char) {
        for(i in 0..strArray.size-1) {
            if(!strArray.get(i).equals(""))
                println("\n"+strArray[i])
        }
    }

    fun traverseAlphabetsWithList(strlist: MutableList<String>) {
        for(i in 0..strlist.size-1) {
            println("\n"+strlist.get(i))
        }
    }
}
fun main(args: Array<String>) {
    val list: MutableList<String> = ArrayList()
    val strArray = CharArray(26)

    var arrayExample = ArrayExample()

    var a : Int = 0
    for(i in 'a'..'z') {
        strArray[a] = i
        a++;
        list.add(i.toString())
    }
    arrayExample.traverseAlphabetsWithArray(*strArray)
    arrayExample.traverseAlphabetsWithList(list)
}